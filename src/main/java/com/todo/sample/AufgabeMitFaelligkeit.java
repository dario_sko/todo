package com.todo.sample;

import exceptions.DatumInVergangenheitException;
import exceptions.TitelLeerException;
import infrastructure.Utility;

import java.io.Serializable;
import java.util.Date;

/**
 * AufgabeMitFaelligkeit erbt von der Superklasse Aufgabe alle Datenfelder sowie Methoden.
 * In dieser Klasse werden alle notwendigen Datenfelder für die Superklasse im (super)Konstruktor zugewiesen
 * und dazu das neue Datenfeld faelligDatum.
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class AufgabeMitFaelligkeit extends Aufgabe implements Serializable {

    private Date faelligDatum;

    public AufgabeMitFaelligkeit(String titel, String text, boolean abgeschlossen, Date faelligDatum) throws TitelLeerException, DatumInVergangenheitException {
        super(titel, text, abgeschlossen);
        setFaelligDatum(faelligDatum);
    }

    /**
     * Setzt das Fälligkeitsdatum der Aufgabe
     *
     * @param datum Fälligkeitsdatum
     * @throws DatumInVergangenheitException - Datum darf nicht in der Vergangenheit liegen
     */
    public void setFaelligDatum(Date datum) throws DatumInVergangenheitException {
        if (datum.before(new Date())) {
            throw new DatumInVergangenheitException();
        } else {
            this.faelligDatum = datum;
        }
    }

    /**
     * Gibt das Fälligkeitsdatum zurück
     *
     * @return Fälligkeitsdatum
     */
    public Date getFaelligDatum() {
        return faelligDatum;
    }

    /**
     * Gibt das Fälligkeitsdatum als formatierter String zurück
     *
     * @return Fälligkeitsdatum dd.MM.yyyy 01.01.2021
     */
    public String getFaelligDatumToString() {
        return Utility.datumFormatierung.format(this.faelligDatum.getTime());
    }

    @Override
    public String toString() {
        return super.toString() +
                " faelligDatum=" + faelligDatum +
                '}';
    }
}
