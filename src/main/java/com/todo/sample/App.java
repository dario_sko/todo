package com.todo.sample;

import exceptions.LadenFehlgeschlagenException;
import exceptions.SpeichernFehlgeschlagenException;
import infrastructure.AufgabenListeSpeicherstand;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ui.GlobalContext;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * Einfaches Programm mit JavaFX GUI für die Verwaltung von normalen Aufgaben und Aufgaben mit Fälligkeitsdatum.
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class App extends Application {

    //Hauptbühne
    public static Stage mainStage;
    // Globale Variable für die Aufgabenliste - GlobalContext
    public static final String GLOBAL_AUFGABENLISTE = "aufgabenliste";

    /**
     * Szene wird auf die übergebene FXML Datei gewechselt.
     *
     * @param fxml      FXML-Datei als String
     * @param resources Resource Bundle als String
     * @throws IOException
     */
    public static void wechsleSzene(String fxml, String resources) throws IOException {
        AnchorPane root = FXMLLoader.load(App.class.getResource(fxml),
                ResourceBundle.getBundle(resources));

        Scene scene = new Scene(root, 850, 375);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * Methode von JavaFX um die GUI zu starten
     *
     * @param stage Welche Bühne gestartet werden soll
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        // Objekt für zum Laden/Speichern der Liste
        AufgabenListeSpeicherstand daten = new AufgabenListeSpeicherstand();

        mainStage = stage;
        // Aufgabenliste wird erstellt
        ToDoListe liste = new ToDoListe();

        // Aufgabenliste wird, falls vorhanden, aus der Datei geladen sonst wird eine neue leere verwendet
        try {
            liste = daten.laden();
        } catch (LadenFehlgeschlagenException e) {
            FehlerDialog(e.getMessage() + " Es wird eine neue Aufgabenliste verwendet.");
            e.printStackTrace();
        }

        // Aufgabenliste wird in den GlobalContext gespeichert um den Datenaustausch
        // zwischen den Klassen und Szene zu erleichtern
        GlobalContext.getGlobalContext().putStateFor(App.GLOBAL_AUFGABENLISTE, liste);

        // Die GUI Szene wird erstellt und angezeigt
        App.wechsleSzene("main.fxml", "com.todo.sample.main");
    }

    /**
     * Beim Beenden der GUI wird diese Methode aufgerufen
     */
    @Override
    public void stop() {
        // Holt sich den momentanen Zustand der Aufgabenliste aus dem GlobalContext
        ToDoListe liste = (ToDoListe) GlobalContext.getGlobalContext().getStateFor(App.GLOBAL_AUFGABENLISTE);

        // Objekt für zum Laden/Speichern der Liste
        AufgabenListeSpeicherstand daten = new AufgabenListeSpeicherstand();

        // Aufgabenliste wird in eine Datei gespeichert
        try {
            daten.speichern(liste);
        } catch (SpeichernFehlgeschlagenException e) {
            App.FehlerDialog("Speichern der Aufgabenliste fehlgeschlagen");
            e.printStackTrace();
        }
    }

    /**
     * Grafischer Popup-Fehlerdialog um dem Benutzer Informationen über den Fehler zu geben
     *
     * @param nachricht Fehlerdetails
     */
    public static void FehlerDialog(String nachricht) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Ein Fehler ist aufgetreten: " + nachricht);
        alert.showAndWait();
    }

    // Die launch Methode für den Start der GUI wird aufgerufen
    public static void main(String[] args) {
        launch(args);
    }
}
