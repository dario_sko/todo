package com.todo.sample;

import exceptions.AufgabeNichtVorhandenException;
import exceptions.AufgabenListeLeerException;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Die Klasse ToDoListe wird verwendet für die Interaktion mit der Klasse Aufgabe.
 *
 * @author dariosko
 * @version 20.06.2021
 */

public class ToDoListe implements Serializable {
    // Datenfeld mit ArrayList vom Objekt Aufgabe
    private ArrayList<Aufgabe> aufgabenliste = new ArrayList();

    /**
     * Konstruktor der eine ArrayList erzeugt.
     */
    public ToDoListe() {
        aufgabenliste = new ArrayList<>();
    }

    /**
     * Fügt der Liste eine neue Aufgabe hinzu
     *
     * @param aufgabe
     * @throws AufgabeNichtVorhandenException Aufgabe != null
     */
    public void hinzufuegen(Aufgabe aufgabe) throws AufgabeNichtVorhandenException {
        if (aufgabe != null) {
            aufgabenliste.add(aufgabe);
        } else {
            throw new AufgabeNichtVorhandenException();
        }
    }

    /**
     * Löscht eine Aufgabe an einem bestimmten Index aus der Liste.
     *
     * @param index an welchem die Aufgabe gelöscht wird
     * @throws AufgabenListeLeerException Falls Liste leer ist
     */
    public void loeschenIndex(int index) throws AufgabenListeLeerException {
        if (aufgabenliste.size() <= 0) {
            aufgabenliste.remove(index);
        } else {
            throw new AufgabenListeLeerException();
        }
    }

    /**
     * Löscht eine Aufgabe aus der Aufgabenliste.
     *
     * @param aufgabe - die gelöscht wird
     * @throws AufgabenListeLeerException Falls Liste leer ist
     */
    public void loeschen(Aufgabe aufgabe) throws AufgabenListeLeerException {
        if (aufgabenliste.size() <= 0) {
            throw new AufgabenListeLeerException();
        } else {
            aufgabenliste.remove(aufgabe);
        }
    }

    /**
     * Liefert die Größe der Aufgabenliste zurück.
     *
     * @return Größe der Liste
     */
    public int size() {
        return aufgabenliste.size();
    }

    /**
     * Liefert die komplette Aufgabenliste zurück
     *
     * @return Aufgabenliste
     */
    public ArrayList<Aufgabe> getAufgabenliste() {
        return aufgabenliste;
    }

}
