package com.todo.sample;

import exceptions.TitelLeerException;
import infrastructure.Utility;

import java.io.Serializable;
import java.util.Date;

/**
 * Die Superklasse Aufgabe wird dazu verwendet die ganzen Datenfelder für eine Aufgabe per Konstruktur zu erstellen und per Setter-Methoden zuzuweisen.
 * Weiteres befinden sich in dieser Klasse Getter-Methoden um Datenfelder Informationen zurückzugeben.
 * Diese Klasse wird in der Klasse ToDoListe als ArrayList verwendet.
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class Aufgabe implements Serializable {

    private String titel;
    private String text;
    private Date erstellungsDatum;
    private boolean abgeschlossen;

    /**
     * Dieser Konstruktor erstellt ein Aufgabe Objekt mit den übergebenen Parametern
     *
     * @param titel wird per Setter-Methode in das Datenfeld titel gespeichert.
     * @param text  wird per Setter-Methode in das Datenfeld text gespeichert.
     *              In das Datenfeld erstellungsDatum wird per Setter-Methode das aktuelle Datum gespeichert.
     *              In das Datenfeld abgeschlossen wird der übergebenen Wert zugewiesen.
     */
    public Aufgabe(String titel, String text, boolean abgeschlossen) throws TitelLeerException {
        this.setTitel(titel);
        this.setText(text);
        this.setErstellungsDatum();
        this.setAbgeschlossen(abgeschlossen);
    }

    /**
     * Gibt das Datenfeld titel als String zurück.
     *
     * @return titel
     */
    public String getTitel() {
        return titel;
    }

    /**
     * Setzt das Datenfeld titel.
     *
     * @param titel
     */
    public void setTitel(String titel) throws TitelLeerException {
        if (!titel.isBlank()) {
            this.titel = titel;
        } else {
            throw new TitelLeerException();
        }
    }

    /**
     * Gibt das Datenfeld text als String zurück.
     *
     * @return text
     */
    public String getText() {
        return text;
    }

    /**
     * Setzt das Datenfeld text.
     *
     * @param text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Gibt den boolean Wert vom Datenfeld abgeschlossen zurück.
     *
     * @return abgeschlossen
     */
    public boolean getAbgeschlossen() {
        return this.abgeschlossen;
    }

    /**
     * Gibt vom Datenfeld abgeschlossen einen String zurück.
     *
     * @return "ja" oder "nein"
     */
    public String getStringAbgeschlossen() {
        return (abgeschlossen ? "ja" : "nein");
    }

    /**
     * Gibt das Erstellungsdatum als String zurück.
     *
     * @return erstellungsDatum
     */
    public Date getErstellungsDatum() {
        return erstellungsDatum;
    }

    public String getErstellungsDatumToString() {
        return Utility.datumFormatierung.format(this.erstellungsDatum.getTime());
    }

    /**
     * Setzt das Datenfeld abgeschlossen auf den übergebenen Wert
     */
    public void setAbgeschlossen(boolean abgeschlossen) {
        if (abgeschlossen) {
            this.abgeschlossen = true;
        } else {
            this.abgeschlossen = false;
        }
    }

    /**
     * Setzt das heutige Datum als Erstellungsdatum
     */
    public void setErstellungsDatum() {
        this.erstellungsDatum = new Date();
    }

    @Override
    public String toString() {
        return "Aufgabe{" +
                "titel='" + titel + '\'' +
                ", text='" + text + '\'' +
                ", erstellungsDatum=" + erstellungsDatum +
                ", abgeschlossen=" + abgeschlossen;
    }
}

