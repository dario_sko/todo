package ui;

import com.todo.sample.App;
import com.todo.sample.Aufgabe;
import com.todo.sample.AufgabeMitFaelligkeit;
import com.todo.sample.ToDoListe;
import exceptions.AufgabeNichtVorhandenException;
import exceptions.DatumInVergangenheitException;
import exceptions.TitelLeerException;
import infrastructure.Utility;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

/**
 * Controller für die Hauptszene
 * Hier ist die Logik für alle Buttons und der Tabelle der GUI enthalten
 */
public class MainController extends BaseController {

    private final String javafxVersion = System.getProperty("javafx.version");

    // Datenfelder für die JavaFX Controls die auf die FXML Datei referenzieren
    @FXML
    private TableView<Aufgabe> tableView;

    @FXML
    private Button btnExit;

    /**
     * JavaFX Methode die beim Aufrufen die Szene initialisiert.
     * Hier baut sie die Tabelle auf und zeigt sie an und gibt dem Verlassen Button eine Logik.
     */
    public void initialize() {

        aufbauenTabelle(this.tableView);
        tabelleAktualisieren(this.tableView);

        btnExit.setOnAction((ActionEvent e) -> {
            Platform.exit();
        });

    }

    /**
     * Anlegen eine Aufgabe die dann in der Aufgabenliste gespeichert wird
     */
    public void anlegenAufgabe() {
        ToDoListe liste = getListe();

        // Titel-Abfrage
        TextInputDialog titelDialog = new TextInputDialog("Aufgabentitel...");
        titelDialog.setTitle("Aufgabentitel eingeben");
        titelDialog.setHeaderText("Wie sollte der Aufgabentitel lauten?");
        titelDialog.setContentText("Aufgabentitel: ");
        Optional<String> ergebnisTitel = titelDialog.showAndWait();

        // Text-Abfrage
        TextInputDialog textDialog = new TextInputDialog("Aufgabentext...");
        textDialog.setTitle("Aufgabentext eingeben");
        textDialog.setHeaderText("Was muss erledigt werden?");
        textDialog.setContentText("Aufgabentext: ");
        Optional<String> ergebnisText = textDialog.showAndWait();

        // Abgeschlossen-Abfrage
        ChoiceDialog<String> abgeschlossenDialog = new ChoiceDialog<>("nein", "ja");
        abgeschlossenDialog.setTitle("Abgeschlossen?");
        abgeschlossenDialog.setHeaderText("Ist es abgeschlossen?");
        abgeschlossenDialog.setContentText("Abgeschlossen?: ");
        Optional<String> ergebnisAbgeschlossen = abgeschlossenDialog.showAndWait();

        boolean abgeschlossen = ergebnisAbgeschlossen.get() == "ja" ? true : false;

        // Fälligkeitsdatum-Abfrage
        boolean faellig = false;
        ChoiceDialog<String> faelligDialog = new ChoiceDialog<>("nein", "ja");
        faelligDialog.setTitle("Fälligkeitsdatum?");
        faelligDialog.setHeaderText("Brauchen sie ein Fälligkeitsdatum?");
        faelligDialog.setContentText("Fälligkeitsdatum: ");
        Optional<String> ergebnisFaellig = faelligDialog.showAndWait();
        faellig = ergebnisFaellig.get() == "ja" ? true : false;

        // Wenn die Abfrage true ist dann wird noch nach dem Fälligkeitsdatum per DatePicker Dialog abgefragt
        if (faellig) {

            // Aufgabe MIT Fälligkeitsdatum
            // Popup-Dialog DatePicker
            final Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(App.mainStage);
            VBox dialogVbox = new VBox(20);
            dialog.setTitle("Fälligkeitsdatum auswählen");
            dialogVbox.getChildren().add(new Text("Wählen Sie das Fälligkeitsdatum aus."));
            DatePicker picker = new DatePicker(null);
            dialogVbox.getChildren().add(picker);
            HBox hBox = new HBox(30);
            Button btnOk = new Button("OK");
            hBox.setAlignment(Pos.CENTER);
            hBox.getChildren().add(btnOk);
            Button btnAbbrechen = new Button("Abbrechen");
            hBox.getChildren().add(btnAbbrechen);
            dialogVbox.getChildren().add(hBox);
            dialogVbox.setAlignment(Pos.CENTER);
            Scene dialogScene = new Scene(dialogVbox, 350, 200);
            dialog.setScene(dialogScene);
            dialog.show();

            final boolean[] ergebnis = {false};

            // OK-Button und Erzeugung einer AufgabeMitFaelligkeit
            final Date[] datum = {null};
            btnOk.setOnAction((ActionEvent a) -> {

                try {
                    datum[0] = Utility.convertToDate(picker.getValue());
                } catch (Exception e) {
                    App.FehlerDialog("Kein Datum ausgewählt!");
                }

                try {
                    liste.hinzufuegen(new AufgabeMitFaelligkeit(ergebnisTitel.get(),
                            ergebnisText.get(),
                            abgeschlossen,
                            datum[0]));
                } catch (AufgabeNichtVorhandenException e) {
                    App.FehlerDialog(e.getMessage());
                    e.printStackTrace();
                } catch (TitelLeerException e) {
                    App.FehlerDialog(e.getMessage());
                    e.printStackTrace();
                } catch (DatumInVergangenheitException e) {
                    App.FehlerDialog(e.getMessage());
                    e.printStackTrace();
                }

                tabelleAktualisieren(this.tableView);
                dialog.close();
            });

            // Abbrechen-Button Logik
            btnAbbrechen.setOnAction((ActionEvent a) -> {
                dialog.close();
            });
        } else {

            // Aufgabe OHNE Fälligkeitsdatum
            try {
                liste.hinzufuegen(new Aufgabe(ergebnisTitel.get(), ergebnisText.get(), abgeschlossen));
            } catch (AufgabeNichtVorhandenException e) {
                App.FehlerDialog(e.getMessage());
                e.printStackTrace();
            } catch (TitelLeerException e) {
                App.FehlerDialog(e.getMessage());
                e.printStackTrace();
            }
        }
        tabelleAktualisieren(tableView);
    }


    /**
     * Wechselt von der Hauptszene in die Bearbeitungsmenü-Szene (2.Szene)
     */
    public void bearbeitungsMenu() {
        try {
            App.wechsleSzene("bearbeiten.fxml", "com.todo.sample.bearbeiten");
        } catch (IOException e) {
            App.FehlerDialog("Kann nicht ins Bearbeitungsmenü wechseln!");
            e.printStackTrace();
        }

    }
}
