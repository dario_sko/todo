package ui;

import java.util.HashMap;

/**
 * Wird verwendet für das Laden und Speichern des aktuellen Zustands der Aufgabenliste somit wird der
 * Datenaustausch zwischen den anderen Klassen mit dieser GlobalContext
 */
public class GlobalContext {


    private static GlobalContext globalContext;
    private HashMap<String, Object> zustand;

    /**
     * Privater Konstruktor - nur von dieser Klasse aus kann ein Objekt erstellt werden
     * erzeugt eine HashMap die Objekte (Value) mit einem String(String) speichern kann.
     */
    private GlobalContext() {
        zustand = new HashMap<>();
    }

    /**
     * Methode um nur ein Objekt von dieser Klasse erstellen zu können um diese zu verwenden
     *
     * @return GlobalContext
     */
    public static GlobalContext getGlobalContext() {
        if (globalContext == null) {
            globalContext = new GlobalContext();
        }
        return globalContext;
    }

    // Einfache Methoden um die Methoden von den HashMap verwenden zu können
    public Object getStateFor(String key) {
        return zustand.get(key);
    }

    public void putStateFor(String key, Object state) {
        zustand.put(key, state);
    }

    public void removeStateFor(String key) {
        zustand.remove(key);
    }

    public void emptyAllStates() {
        zustand.clear();
    }
}
