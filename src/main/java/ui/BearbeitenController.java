package ui;

import com.todo.sample.App;
import com.todo.sample.Aufgabe;
import com.todo.sample.AufgabeMitFaelligkeit;
import com.todo.sample.ToDoListe;
import exceptions.AufgabeNichtVorhandenException;
import exceptions.AufgabenListeLeerException;
import exceptions.DatumInVergangenheitException;
import exceptions.TitelLeerException;
import infrastructure.Utility;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;

/**
 * Controller für die 2.Szene das Bearbeitungsmenü
 * Hier ist die Logik für alle Buttons und der Tabelle der GUI enthalten
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class BearbeitenController extends BaseController {

    // Datenfelder für die JavaFX Controls die auf die FXML Datei referenzieren
    @FXML
    private Button btnBack;
    @FXML
    private DatePicker datePicker;
    @FXML
    private TableView<Aufgabe> tableView;

    /**
     * JavaFX Methode die beim Aufrufen die Szene initialisiert.
     * Hier baut sie die Tabelle auf und zeigt sie an und gibt dem Zurück Button eine Logik.
     */
    public void initialize() {
        aufbauenTabelle(this.tableView);
        tabelleAktualisieren(this.tableView);

        // zurück Button
        btnBack.setOnAction((ActionEvent e) ->
        {
            try {
                App.wechsleSzene("main.fxml", "com.todo.sample.main");
            } catch (IOException ioException) {
                App.FehlerDialog("Kann nicht ins Hauptfenster wechseln!");
                ioException.printStackTrace();
            }
        });
    }

    /**
     * Ändert den Titel der markierten Aufgabe in der Tabelle
     */
    public void aendernTitel() {
        Aufgabe aufgabe = this.tableView.getSelectionModel().getSelectedItem();
        if (aufgabe == null) {
            App.FehlerDialog("Wählen Sie bitte die Aufgabe aus die Sie bearbeiten möchten.");
        } else {
            TextInputDialog titelDialog = new TextInputDialog("Aufgabentitel ändern...");
            titelDialog.setTitle("Aufgabentitel ändern");
            titelDialog.setHeaderText("Wie sollte der neue Aufgabentitel lauten?");
            titelDialog.setContentText("Neuer Aufgabentitel: ");
            Optional<String> ergebnisTitel = titelDialog.showAndWait();

            try {
                aufgabe.setTitel(ergebnisTitel.get());
            } catch (TitelLeerException e) {
                App.FehlerDialog(e.getMessage());
                e.printStackTrace();
            }

            tabelleAktualisieren(this.tableView);
        }

    }

    /**
     * Ändert den Text der markierten Aufgabe in der Tabelle
     */
    public void aendernText() {
        Aufgabe aufgabe = this.tableView.getSelectionModel().getSelectedItem();
        if (aufgabe == null) {
            App.FehlerDialog("Wählen Sie bitte die Aufgabe aus die Sie bearbeiten möchten.");
        } else {
            TextInputDialog textDialog = new TextInputDialog("Aufgabentext ändern...");
            textDialog.setTitle("Aufgabentext ändern");
            textDialog.setHeaderText("Wie sollte der neue Aufgabentext lauten?");
            textDialog.setContentText("Neuer Aufgabentext: ");
            Optional<String> ergebnisText = textDialog.showAndWait();

            aufgabe.setText(ergebnisText.get());

            tabelleAktualisieren(this.tableView);
        }

    }

    /**
     * Setzt die markierte Aufgabe in der Tabelle als abgeschlossen
     */
    public void abgeschlossen() {
        Aufgabe aufgabe = this.tableView.getSelectionModel().getSelectedItem();
        if (aufgabe == null) {
            App.FehlerDialog("Wählen Sie bitte die Aufgabe aus die Sie bearbeiten möchten.");
        } else {
            aufgabe.setAbgeschlossen(true);
            tabelleAktualisieren(this.tableView);
        }

    }

    /**
     * Setzt die markierte Aufgabe in der Tabelle als nicht abgeschlossen
     */
    public void nichtAbgeschlossen() {
        Aufgabe aufgabe = this.tableView.getSelectionModel().getSelectedItem();
        if (aufgabe == null) {
            App.FehlerDialog("Wählen Sie bitte die Aufgabe aus die Sie bearbeiten möchten.");
        } else {
            aufgabe.setAbgeschlossen(false);
            tabelleAktualisieren(this.tableView);
        }

    }

    /**
     * Löscht die markierte Aufgabe in der Tabelle aus der Aufgabenliste
     */
    public void loeschenAufgabe() {
        ToDoListe liste = getListe();
        Aufgabe aufgabe = this.tableView.getSelectionModel().getSelectedItem();
        if (aufgabe == null) {
            App.FehlerDialog("Wählen Sie bitte die Aufgabe aus die Sie bearbeiten möchten.");
        } else {
            try {
                liste.loeschen(aufgabe);
            } catch (AufgabenListeLeerException e) {
                App.FehlerDialog(e.getMessage());
                e.printStackTrace();
            }
            tabelleAktualisieren(this.tableView);
        }
    }

    /**
     * Ändert das Fälligkeitsdatum der markierten Aufgabe in der Tabelle per DatePicker
     * Falls die Aufgabe eine normale Aufgabe ist wird sie gelöscht und eine neue AufgabeMitFaelligkeit erstellt
     * und der Liste hinzugefügt
     */
    public void aendernFaelligkeitsdatum() {
        ToDoListe liste = getListe();
        Aufgabe aufgabe = this.tableView.getSelectionModel().getSelectedItem();
        Date datum = null;

        try {
            datum = Utility.convertToDate(this.datePicker.getValue());
        } catch (Exception e) {
            App.FehlerDialog("Kein Datum ausgewählt");
        }

        if (aufgabe == null) {
            App.FehlerDialog("Wählen Sie bitte die Aufgabe aus die Sie bearbeiten möchten.");
        } else {
            try {
                liste.hinzufuegen(
                        new AufgabeMitFaelligkeit(aufgabe.getTitel(),
                                aufgabe.getText(),
                                aufgabe.getAbgeschlossen(),
                                datum));

                liste.loeschen(aufgabe);
            } catch (DatumInVergangenheitException e) {
                App.FehlerDialog(e.getMessage());
                e.printStackTrace();
            } catch (TitelLeerException e) {
                App.FehlerDialog(e.getMessage());
                e.printStackTrace();
            } catch (AufgabeNichtVorhandenException e) {
                App.FehlerDialog(e.getMessage());
                e.printStackTrace();
            } catch (AufgabenListeLeerException e) {
                App.FehlerDialog(e.getMessage());
                e.printStackTrace();
            }
        }

        tabelleAktualisieren(this.tableView);
    }
}

