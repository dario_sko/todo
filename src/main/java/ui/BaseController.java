package ui;

import com.todo.sample.App;
import com.todo.sample.Aufgabe;
import com.todo.sample.ToDoListe;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Superklasse BaseController für alle Controller um den momentanen Zustand der Aufgabenliste
 * speichern und abrufen zu können.
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class BaseController {

    private ToDoListe liste = (ToDoListe) GlobalContext.getGlobalContext().getStateFor(App.GLOBAL_AUFGABENLISTE);

    /**
     * Gibt momentanen Zustand der Aufgabenliste zurück
     *
     * @return
     */
    public ToDoListe getListe() {
        return this.liste;
    }

    /**
     * Speichert die übergebene Aufgabenliste in den GlobalContext
     *
     * @param liste
     */
    public void setListe(ToDoListe liste) {
        GlobalContext.getGlobalContext().putStateFor(App.GLOBAL_AUFGABENLISTE, liste);

    }

    /**
     * Baut die Tabelle für die GUI auf
     *
     * @param tableView TableView Referenz auf die FXML vom Controller
     */
    public void aufbauenTabelle(TableView tableView) {
        ToDoListe liste = getListe();

        TableColumn<Aufgabe, String> titel = new TableColumn<>("TITEL");
        titel.setCellValueFactory(new PropertyValueFactory<>("titel"));
        TableColumn<Aufgabe, String> text = new TableColumn<>("TEXT");
        text.setCellValueFactory(new PropertyValueFactory<>("text"));
        TableColumn<Aufgabe, String> erstellungsDatum = new TableColumn<>("ERSTELLUNGSDATUM");
        erstellungsDatum.setCellValueFactory(new PropertyValueFactory<>("ErstellungsDatumToString"));
        TableColumn<Aufgabe, String> abgeschlossen = new TableColumn<>("ABGESCHLOSSEN");
        abgeschlossen.setCellValueFactory(new PropertyValueFactory<>("StringAbgeschlossen"));
        TableColumn<Aufgabe, String> faelligDatum = new TableColumn<>("FÄLLIGKEITSDATUM");
        faelligDatum.setCellValueFactory(new PropertyValueFactory<>("faelligDatumToString"));

        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getColumns().clear();
        tableView.getColumns().add(titel);
        tableView.getColumns().add(text);
        tableView.getColumns().add(erstellungsDatum);
        tableView.getColumns().add(abgeschlossen);
        tableView.getColumns().add(faelligDatum);

        tabelleAktualisieren(tableView);
    }

    /**
     * Aktualisiert die Tabellendaten mit dem aktuellen Zustand
     *
     * @param tableView TableView Referenz auf die FXML vom Controller
     */
    public void tabelleAktualisieren(TableView tableView) {
        ToDoListe liste = getListe();
        tableView.getItems().setAll(liste.getAufgabenliste());
    }
}
