package exceptions;

/**
 * Exception falls Aufgabe nicht vorhanden.
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class AufgabeNichtVorhandenException extends Exception {

    public AufgabeNichtVorhandenException() {
        super("Aufgabe nicht vorhanden!");
    }
}
