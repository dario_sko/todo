package exceptions;

/**
 * Exception falls der Titel leer ist
 *
 * @author dariosko
 * @version 20.06.2021
 */

public class TitelLeerException extends Exception {

    public TitelLeerException() {
        super("Titel darf nicht leer sein!");
    }
}
