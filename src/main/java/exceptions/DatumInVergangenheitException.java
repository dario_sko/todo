package exceptions;

/**
 * Exception falls Datum in der Vergangenheit liegt
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class DatumInVergangenheitException extends Exception {

    public DatumInVergangenheitException() {
        super("Datum in der Vergangenheit!");
    }
}
