package exceptions;

/**
 * Exception falls Speichern der Datei fehlschlägt
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class SpeichernFehlgeschlagenException extends Exception {

    public SpeichernFehlgeschlagenException() {
        super("Speichern der Aufgabenliste fehlgeschlagen!");
    }
}
