package exceptions;

/**
 * Exception falls das Laden der Datei fehlschlägt
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class LadenFehlgeschlagenException extends Exception {

    public LadenFehlgeschlagenException() {
        super("Laden der Aufgabenliste fehlgeschlagen!");
    }
}
