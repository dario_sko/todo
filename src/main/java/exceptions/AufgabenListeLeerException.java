package exceptions;

/**
 * Exception falls Aufgabeliste leer ist
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class AufgabenListeLeerException extends Exception {

    public AufgabenListeLeerException() {
        super("Aufgabenliste ist leer!");
    }
}
