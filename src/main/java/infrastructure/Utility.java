package infrastructure;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Klasse für Hilfsmethoden
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class Utility {

    /**
     * Formatierung vom Datum
     */
    public static SimpleDateFormat datumFormatierung = new SimpleDateFormat("dd.MM.yyyy");

    /**
     * Fürs konvertieren von LocalDate zu Date
     *
     * @param localDate LocalDate
     * @return Date
     */
    public static Date convertToDate(LocalDate localDate) {
        return java.util.Date.from(localDate.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }


}
