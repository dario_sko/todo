package infrastructure;

import com.todo.sample.ToDoListe;
import exceptions.LadenFehlgeschlagenException;
import exceptions.SpeichernFehlgeschlagenException;

import java.io.*;

/**
 * Klasse für das Laden und Speichern der Aufgabenliste
 *
 * @author dariosko
 * @version 20.06.2021
 */
public class AufgabenListeSpeicherstand implements LadenUndSpeichern {

    /**
     * Speichert die Aufgabenliste in eine binäre Datei (.bin)
     *
     * @param toDoListe Aufgabenliste
     * @throws SpeichernFehlgeschlagenException falls das Speichern NICHT erfolgreich ist
     */
    @Override
    public void speichern(ToDoListe toDoListe) throws SpeichernFehlgeschlagenException {
        if (toDoListe != null) {
            try {
                ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("aufgabenliste.bin"));
                output.writeObject(toDoListe);
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw new SpeichernFehlgeschlagenException();
            }
        }
    }

    /**
     * Ladet aus der binären Datei (.bin) die Aufgabenliste
     *
     * @return Aufgabenliste
     * @throws LadenFehlgeschlagenException falls das Laden NICHT erfolgreich ist
     */
    @Override
    public ToDoListe laden() throws LadenFehlgeschlagenException {
        ToDoListe liste = null;
        try {
            ObjectInputStream input = new ObjectInputStream(new FileInputStream("aufgabenliste.bin"));
            liste = (ToDoListe) input.readObject();
            input.close();
            return liste;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new LadenFehlgeschlagenException();
        }
    }

}
