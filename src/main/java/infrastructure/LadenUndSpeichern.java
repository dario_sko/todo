package infrastructure;

import com.todo.sample.ToDoListe;
import exceptions.LadenFehlgeschlagenException;
import exceptions.SpeichernFehlgeschlagenException;

/**
 * Interface für Laden und Speichern
 *
 * @author dariosko
 * @version 20.06.2021
 */
public interface LadenUndSpeichern {

    void speichern(ToDoListe toDoListe) throws SpeichernFehlgeschlagenException;

    ToDoListe laden() throws LadenFehlgeschlagenException;
}
